from django.shortcuts import render,redirect
from gestionVentas.models import Producto,Categoria
from gestionVentas.forms import ProductoForm,CategoriaForm
# Create your views here.

# Vista para hacer consulta
def consultaProducto(request):
    productos = Producto.objects.all()
    return render(request,'productos.html',{'listProductos':productos})

# Vista para crear Categoria
def crearCategoria(request):
    if request.method == 'POST':
        categoria_form = CategoriaForm(request.POST)
        if categoria_form.is_valid():
            categoria_form.save()
            return redirect('home')
    else:
        categoria_form = CategoriaForm()
    return render(request,'crearCategoria',{'categoria_form':categoria_form})

# Vista para editar
def editarProducto(request,id):
    producto = Producto.objects.get(id=id)
    if request.method == 'GET':
        producto_form = ProductoForm(instance=producto)
    else:
        producto_form = ProductoForm(request.POST, instance=producto)
        if producto_form.is_valid():
            producto_form.save()
        redirect('consultaProducto/')
    return render(request,'consultaProducto.html',{'producto_form':producto_form})

def home(request):
    return render(request,'index.html')

def categoria(request):
    categoria = Categoria.objects.all()
    listCategoria_form = []
    for unidadCategoria in categoria:
        categoria_form = CategoriaForm()
        listCategoria_form.append(categoria_form)
    return render(request, 'categoria.html',{'listCategoria':categoria,'listCategoria_form':listCategoria_form})
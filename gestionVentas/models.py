from django.db import models

# Create your models here.
class Categoria(models.Model):
    id_categoria = models.AutoField(primary_key = True)
    nombre = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=30)

    def __str__(self):
        return self.nombre


class Producto(models.Model):
    id_producto=models.AutoField(primary_key=True)
    nombre=models.CharField(max_length=30)
    precio=models.FloatField()
    stock=models.IntegerField()
    id_categoria=models.ForeignKey(Categoria, on_delete= models.CASCADE)
        
    def __str__(self):
        return self.nombre

class Cabecera_Venta(models.Model):
    num_Venta=models.AutoField(primary_key=True)
    fecha = models.DateField()

    def __str__(self):
        return self.num_Venta

class Detalle_Venta(models.Model):
    num_Detalle = models.AutoField(primary_key=True)
    num_Venta = models.ForeignKey(Cabecera_Venta, on_delete=models.CASCADE)
    id_producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    cantidad = models.IntegerField()

    def __str__(self):
        return self.num_Venta + ':' + self.num_Detalle